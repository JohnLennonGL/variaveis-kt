package com.jlngls.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //variavel mutavel

        var usuario = "John Lennon"
        usuario = "Isuku"
        Log.v("USUARIO",usuario)

        


        var salario = 1000
        var horasExtras = 200

        var total = salario + horasExtras

        // Variavel imutavel

        val pi = 3.14
        pi = 3.0 // nao da pra mudar o valor acima. por isso da erro
    }
}